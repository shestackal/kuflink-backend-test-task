@extends('layouts.app')

@section('pagespecificscripts')
    <script src="{{ asset('/js/calculator.js') }}" defer></script>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">{{ __('Calculator') }}</div>

                    <div class="card-body">
                        <form id="calculator_form" method="POST" action="{{ route('calculator.process') }}">

                            <div class="form-group row">
                                <div class="col-md-3">
                                    <div class="form-group input-group">
                                        <input type="number" class="form-control @error('first_argument') is-invalid @enderror"
                                            id="first_argument" name="first_argument" min="-99999" max="99999" step="1" required
                                            value="{{ old('first_argument') }}" placeholder="{{ __('First argument') }}" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group input-group">
                                        <select class="custom-select form-control @error('action') is-invalid @enderror"
                                            id="action" name="action" required>
                                            @foreach ($actions as $action => $actionItem)
                                                @php
                                                    $actionTitle = data_get($actionItem, 'title');
                                                    $actionIcon = data_get($actionItem, 'icon');
                                                @endphp
                                                <option value="{{ $action }}" {{ $action === old('action') ? 'selected' : '' }}>
                                                    {{ sprintf('%1s - %2s (%3s)', $actionIcon, $actionTitle, $action) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group input-group">
                                        <input type="number" class="form-control @error('second_argument') is-invalid @enderror"
                                            id="second_argument" name="second_argument" min="-99999" max="99999" step="1" required
                                            value="{{ old('second_argument') }}" placeholder="{{ __('Second argument') }}" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group input-group">
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-equals">=</i>
                                                </span>
                                            </div>
                                            <input type="number" class="form-control" id="result" name="result" disabled value=""
                                                placeholder="{{ __('Result') }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-success pull-right">
                                        {{ __('Calculate') }}
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
