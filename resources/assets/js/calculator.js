$(document).ready(function () {

    // Ajax for our form
    $('form[id="calculator_form"]').on('submit', function(event) {
        event.preventDefault();

        var formData = {
            first_argument: $('input[name=first_argument]').val(),
            second_argument: $('input[name=second_argument]').val(),
            action: $('select[name=action]').val(),
        }

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            success: function(response) {
                $('form[id="calculator_form"] input[id="result"]').val(response.result);
                AddMessage('success', response.message);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                AddMessage('error', xhr.responseJSON.message);
            }
        })

        return false;
    });

    // adds message by message type and text
    function AddMessage(type, message) {
        ClearMessages();

        var messageSection = $('[data-section="messages"]');
        var messageClass = type == 'success' ? 'alert-success' : 'alert-danger';
        var messageItem = messageSection.find('div.alert.' + messageClass);

        if (messageItem.length == 0) {
            messageItem = $('<div></div>', { 'class': 'alert ' + messageClass });
            messageSection.append(messageItem);
        } else {
            messageItem.append($('<br />'));
        }

        messageItem.append(message);
    }

    // adds message by message type and text
    function ClearMessages(type) {
        var messageSection = $('[data-section="messages"]');

        var messageClass = '';
        switch (type) {
            case 'success':
                messageClass = '.alert-success';
                break;
            case 'error':
                messageClass = '.alert-danger';
                break;
            default:
                break;
        }

        messageSection.find('div.alert' + messageClass).empty().remove();
    }

});
