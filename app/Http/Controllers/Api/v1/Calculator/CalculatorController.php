<?php

namespace App\Http\Controllers\Api\v1\Calculator;

use App\Http\Requests\Api\Calculator\CalculatorRequest;
use App\Http\Controllers\Controller;
use App\Utilites\Calculator\Contracts\CalculatorService;
use App\Utilites\Calculator\Exceptions\BuildActionHandlerException;

class CalculatorController extends Controller
{

    /** @var CalculatorService $calculatorService */
    private $calculatorService;

    /**
     * Controller constructor.
     *
     * @param CalculatorService $calculatorService
     */
    public function __construct(CalculatorService $calculatorService)
    {
        $this->calculatorService = $calculatorService;
    }

    /**
     * Returns calculation result
     *
     * @param CalculatorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function calculate(CalculatorRequest $request)
    {
        /** @var string $actionType */
        $actionType = $request->get(CalculatorRequest::ACTION);

        /** @var float $firstArgument */
        $firstArgument = (float)$request->get(CalculatorRequest::FIRST_ARGUMENT);

        /** @var float $secondArgument */
        $secondArgument = (float)$request->get(CalculatorRequest::SECOND_ARGUMENT);

        try {
            /** @var float $result */
            $result = $this->calculatorService
                ->calculate(
                    $actionType,
                    $firstArgument,
                    $secondArgument
                );
        } catch (BuildActionHandlerException $exception) {
            return response()->error([
                'message' => trans('messages.error_calculations'),
            ]);
        }

        return response()->success([
            'message' => trans('messages.success_calculations'),
            'result' => sprintf('%01.2f', $result),
        ]);
    }

}
