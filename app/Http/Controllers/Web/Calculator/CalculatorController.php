<?php

namespace App\Http\Controllers\Web\Calculator;

use App\Utilites\Calculator\Contracts\ActionsService;
use App\Http\Controllers\Controller;

class CalculatorController extends Controller
{

    /** @var ActionsService $actionsService */
    private $actionsService;

    /**
     * Controller constructor.
     *
     * @param ActionsService $actionsService
     */
    public function __construct(ActionsService $actionsService)
    {
        $this->actionsService = $actionsService;
    }

    /**
     * Shows calculator form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /** @var array $actions */
        $actions = $this->actionsService->getActionsConfig();

        return view('calculator.form', compact(['actions', ]));
    }

}
