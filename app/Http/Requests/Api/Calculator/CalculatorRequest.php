<?php

namespace App\Http\Requests\Api\Calculator;

use App\Http\Requests\Contracts\Calculator\CalculatorRequest as CalculatorRequestContract;
use App\Http\Requests\Api\Request;
use App\Utilites\Calculator\Contracts\ActionsService;

class CalculatorRequest extends Request implements CalculatorRequestContract
{

    /** @var ActionsService $actionsService */
    private $actionsService;

    /**
     * Controller constructor.
     *
     * @param ActionsService $actionsService
     */
    public function __construct(ActionsService $actionsService)
    {
        $this->actionsService = $actionsService;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var string $actionTypes */
        $actionTypes = implode(',', $this->actionsService->getActionsTypes());

        return [
            self::FIRST_ARGUMENT  => sprintf('required|numeric|min:%01.2f|max:%01.2f', -99999, 99999),
            self::SECOND_ARGUMENT => sprintf('required|numeric|min:%01.2f|max:%01.2f', -99999, 99999),
            self::ACTION          => sprintf('required|in:%1s', $actionTypes),
        ];
    }

}
