<?php

namespace App\Http\Requests\Contracts\Calculator;

interface CalculatorRequest
{
    const FIRST_ARGUMENT = 'first_argument';
    const SECOND_ARGUMENT = 'second_argument';
    const ACTION = 'action';

}
