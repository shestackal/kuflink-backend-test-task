<?php

namespace App\Providers;

use App\Utilites\Calculator\Contracts\ActionsService;
use App\Utilites\Calculator\Contracts\CalculatorService;
use Illuminate\Support\ServiceProvider;

class UtilitesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //ActionsService
        $this->app->bind(
            ActionsService::class,
            \App\Utilites\Calculator\ActionsService::class
        );

        //CalculatorService
        $this->app->bind(
            CalculatorService::class,
            \App\Utilites\Calculator\CalculatorService::class
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
