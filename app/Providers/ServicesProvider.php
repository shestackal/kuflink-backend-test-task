<?php

namespace App\Providers;

use App\Src\User\Contracts\UserManageService;
use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //UserManageService
        $this->app->bind(
            UserManageService::class,
            \App\Src\User\UserManageService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
