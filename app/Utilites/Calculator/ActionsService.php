<?php

namespace App\Utilites\Calculator;

use App\Utilites\Adapters\Config\Contracts\ConfigRepository;
use App\Utilites\Calculator\Contracts\ActionsService as ActionsServiceContract;

/**
 * Class ActionsService
 * @package App\Utilites\Calculator
 */
class ActionsService implements ActionsServiceContract
{

    const CALC_ACTIONS_CONFIG = 'calculator.actions';

    /** @var ConfigRepository $configRepository */
    private $configRepository;

    /** @var array $actionsConfig */
    private $actionsConfig;

    /**
     * Service constructor.
     *
     * @param ConfigRepository $configRepository
     */
    public function __construct(ConfigRepository $configRepository)
    {
        $this->configRepository = $configRepository;
        $this->actionsConfig = $this->getConfig();
    }

    /**
     * Returns configs for calculator's actions
     *
     * @return array
     */
    public function getActionsConfig(): array
    {
        return $this->actionsConfig;
    }

    /**
     * Returns classname of action handler by provided action type
     *
     * @param string $actionType
     * @return string|null
     */
    public function getActionHandlerByType(string $actionType): ?string
    {
        $actionItem = data_get($this->actionsConfig, $actionType, []);

        return data_get($actionItem, 'handler');
    }

    /**
     * Returns available actions' types
     *
     * @return array
     */
    public function getActionsTypes(): array
    {
        return array_keys($this->actionsConfig);
    }

    /**
     * Gets actions' configs
     *
     * @return array
     */
    private function getConfig(): array
    {
        return $this->configRepository->get(self::CALC_ACTIONS_CONFIG) ?? [];
    }

}
