<?php

namespace App\Utilites\Calculator\Actions;

use App\Utilites\Calculator\Actions\Contracts\CalculatorAction;

/**
 * Class Division
 * @package App\Utilites\Calculator\Actions
 */
final class Division implements CalculatorAction
{

    /**
     * Calculates by provided data
     *
     * @return float
     */
    public function calculate(float $firstArgument, float $secondArgument): float
    {
        return (float)($firstArgument / $secondArgument);
    }

}
