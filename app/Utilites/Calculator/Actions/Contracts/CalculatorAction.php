<?php

namespace App\Utilites\Calculator\Actions\Contracts;

interface CalculatorAction
{

    /**
     * Calculates by provided data
     *
     * @return float
     */
    public function calculate(float $firstArgument, float $secondArgument): float;

}
