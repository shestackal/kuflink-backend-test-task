<?php

namespace App\Utilites\Calculator\Contracts;

interface CalculatorService
{

    /**
     * Calculates by provided data
     *
     * @param string $actionType
     * @param float $firstArgument
     * @param float $secondArgument
     * @return float
     */
    public function calculate(string $actionType, float $firstArgument, float $secondArgument): float;

}
