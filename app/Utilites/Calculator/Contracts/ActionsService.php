<?php

namespace App\Utilites\Calculator\Contracts;

interface ActionsService
{

    /**
     * Returns configs for calculator's actions
     *
     * @return array
     */
    public function getActionsConfig(): array;

    /**
     * Returns classname of action handler by provided action type
     *
     * @param string $actionType
     * @return string|null
     */
    public function getActionHandlerByType(string $actionType): ?string;

    /**
     * Returns available actions' types
     *
     * @return array
     */
    public function getActionsTypes(): array;

}
