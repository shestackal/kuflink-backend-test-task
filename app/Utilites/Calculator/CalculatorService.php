<?php

namespace App\Utilites\Calculator;

use App\Utilites\Adapters\Container\Contracts\Container;
use App\Utilites\Calculator\Actions\Contracts\CalculatorAction;
use App\Utilites\Calculator\Contracts\ActionsService;
use App\Utilites\Calculator\Contracts\CalculatorService as CalculatorServiceContract;
use App\Utilites\Calculator\Exceptions\BuildActionHandlerException;

/**
 * Class ActionsService
 * @package App\Utilites\Calculator
 */
class CalculatorService implements CalculatorServiceContract
{

    /** @var Container $container */
    private $container;

    /** @var ActionsService $actionsService */
    private $actionsService;

    /**
     * Controller constructor.
     *
     * @param Container $container
     * @param ActionsService $actionsService
     */
    public function __construct(Container $container, ActionsService $actionsService)
    {
        $this->container = $container;
        $this->actionsService = $actionsService;
    }

    /**
     * Calculates by provided data
     *
     * @param string $actionType
     * @param float $firstArgument
     * @param float $secondArgument
     * @return float
     * @throws BuildActionHandlerException
     */
    public function calculate(string $actionType, float $firstArgument, float $secondArgument): float
    {
        /** @var CalculatorAction $handlerInstance */
        $handlerInstance = $this->getActionHandlerByType($actionType);

        /** @var float $result */
        $result = $handlerInstance->calculate($firstArgument, $secondArgument);

        return $result;
    }

    /**
     * Returns instance of action handler by provided action type
     *
     * @param string $actionType
     * @return CalculatorAction
     * @throws BuildActionHandlerException
     */
    private function getActionHandlerByType(string $actionType): CalculatorAction
    {
        /** @var string|null $handler */
        $handler = $this->actionsService->getActionHandlerByType($actionType);

        if (!$handler) {
            throw new BuildActionHandlerException(sprintf(
                'Cannot find calculator action handler instance by given type - %s',
                $handler
            ));
        }

        try {
            /** @var CalculatorAction $handlerInstance */
            $handlerInstance = $this->container->make($handler);
        } catch (\Exception $exception) {
            throw new BuildActionHandlerException(sprintf(
                'Cannot build calculator action handler instance by given type - %s, %s',
                $handler,
                $exception->getMessage()
            ));
        }

        return $handlerInstance;
    }

}
