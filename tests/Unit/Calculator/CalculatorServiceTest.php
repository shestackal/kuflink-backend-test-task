<?php

namespace Tests\Unit\Calculator;

use App\Utilites\Adapters\Container\Contracts\Container;
use App\Utilites\Calculator\Actions\Contracts\CalculatorAction;
use App\Utilites\Calculator\CalculatorService;
use App\Utilites\Calculator\Contracts\ActionsService;
use App\Utilites\Calculator\Exceptions\BuildActionHandlerException;
use Mockery;
use Tests\TestCase;

class CalculatorServiceTest extends TestCase
{

    /**
     * Checks is getActionHandlerByType method is called
     */
    public function testHandle_RandomData_GetActionHandlerByTypeIsCalled()
    {
        // Mock action handler
        $actionMock = Mockery::mock(CalculatorAction::class);
        $actionMock->allows('calculate')
            ->andReturn(1.01);

        // Mock container handler
        $containerMock = Mockery::mock(Container::class);
        $containerMock->allows('make')
            ->andReturn($actionMock);

        // Mock actions' service
        $actionsServiceMock = Mockery::mock(ActionsService::class);
        $actionsServiceMock->shouldReceive('getActionHandlerByType')
            ->andReturn('action')
            ->once();

        $service = new CalculatorService($containerMock, $actionsServiceMock);

        // Mock calculator service
        $serviceMock = Mockery::mock(CalculatorService::class, $service);

        // Execute test data
        $serviceMock->calculate('action', 1.01, 1.01);
    }

    /**
     * Checks is container's make method is called
     */
    public function testHandle_RandomData_ContainerMakeIsCalled()
    {
        // Mock action handler
        $actionMock = Mockery::mock(CalculatorAction::class);
        $actionMock->allows('calculate')
            ->andReturn(1.01);

        // Mock container handler
        $containerMock = Mockery::mock(Container::class);
        $containerMock->shouldReceive('make')
            ->andReturn($actionMock)
            ->once();

        // Mock actions' service
        $actionsServiceMock = Mockery::mock(ActionsService::class);
        $actionsServiceMock->allows('getActionHandlerByType')
            ->andReturn('action');

        $service = new CalculatorService($containerMock, $actionsServiceMock);

        // Mock calculator service
        $serviceMock = Mockery::mock(CalculatorService::class, $service);

        // Execute test data
        $serviceMock->calculate('action', 1.01, 1.01);
    }

    /**
     * Checks is BuildActionHandlerException fired on not found action type
     */
    public function testHandle_RandomData_ActionNotFound()
    {
        // Mock action handler
        $actionMock = Mockery::mock(CalculatorAction::class);
        $actionMock->allows('calculate')
            ->andReturn(1.01);

        // Mock container handler
        $containerMock = Mockery::mock(Container::class);
        $containerMock->allows('make')
            ->andReturn($actionMock);

        // Mock actions' service
        $actionsServiceMock = Mockery::mock(ActionsService::class);
        $actionsServiceMock->allows('getActionHandlerByType')
            ->andReturn(null);

        $service = new CalculatorService($containerMock, $actionsServiceMock);

        // Mock calculator service
        $serviceMock = Mockery::mock(CalculatorService::class, $service);

        $this->expectException(BuildActionHandlerException::class);

        // Execute test data
        $serviceMock->calculate('action', 1.01, 1.01);
    }

    /**
     * Checks is calculate method is called
     */
    public function testHandle_RandomData_CalculateIsCalled()
    {
        // Mock action handler
        $actionMock = Mockery::mock(CalculatorAction::class);
        $actionMock->shouldReceive('calculate')
            ->andReturn(1.01)
            ->once();

        // Mock container handler
        $containerMock = Mockery::mock(Container::class);
        $containerMock->allows('make')
            ->andReturn($actionMock);

        // Mock actions' service
        $actionsServiceMock = Mockery::mock(ActionsService::class);
        $actionsServiceMock->allows('getActionHandlerByType')
            ->andReturn('action');

        $service = new CalculatorService($containerMock, $actionsServiceMock);

        // Mock calculator service
        $serviceMock = Mockery::mock(CalculatorService::class, $service);

        // Execute test data
        $serviceMock->calculate('action', 1.01, 1.01);
    }

}
