<?php

return [
    'addition' => [
        'title' => 'Alien',
        'icon' => Spatie\Emoji\Emoji::CHARACTER_ALIEN,
        'handler' => \App\Utilites\Calculator\Actions\Addition::class,
    ],
    'subtraction' => [
        'title' => 'Skull',
        'icon' => Spatie\Emoji\Emoji::CHARACTER_SKULL,
        'handler' => \App\Utilites\Calculator\Actions\Subtraction::class,
    ],
    'multiplication' => [
        'title' => 'Ghost',
        'icon' => Spatie\Emoji\Emoji::CHARACTER_GHOST,
        'handler' => \App\Utilites\Calculator\Actions\Multiplication::class,
    ],
    'division' => [
        'title' => 'Scream',
        'icon' => Spatie\Emoji\Emoji::CHARACTER_FACE_SCREAMING_IN_FEAR,
        'handler' => \App\Utilites\Calculator\Actions\Division::class,
    ],
];
